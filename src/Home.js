import { useHistory } from "react-router";
import { useAuth } from "./app/privateRoute";

const Home = () => {
  let history = useHistory();
  let auth = useAuth();

  return(
    <div>
      <h1>Home page</h1>
      <p>Slotmachine is an relaxing game where you just need to spin the reels and have luck to win some coins. The game is pretty rewarding but it's fun.</p>
      <p>You can register for free and enjoy this great game. Just don't get an gambling addiction.</p>
      <button className="btn btn-primary" onClick={() => auth.signout(() => { history.replace('/login') })}>Logout</button>
    </div>
  );
}

export default Home;
