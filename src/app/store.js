import { configureStore } from '@reduxjs/toolkit';
import slotReducer from '../features/slot/slotSlice';
import userReducer from '../features/user/userSlice';

export const store = configureStore({
  reducer: {
    slot: slotReducer,
    user: userReducer,
  },
});
