import { useContext, createContext } from 'react';
import { Redirect, Route } from 'react-router';

export const authContext = createContext();

export const useAuth = () => {
  return useContext(authContext);
}

const PrivateRoute = ({ children, ...rest }) => {
  let auth = useAuth();

  return (
    <Route
      {...rest}
      render={({ location }) =>
        auth.isLoggedIn ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

export default PrivateRoute;
