import { useDispatch, useSelector } from "react-redux";
import { login, logout } from "../features/user/userSlice";
import { authContext } from "./privateRoute";

const useProvideAuth = () => {
  const {
    isLoggedIn,
  } = useSelector(state => state.user.value);

  const dispatch = useDispatch();

  const signin = ({username, password}) => {
    return dispatch(login({username, password}));
  };

  const signout = cb => {
    dispatch(logout());
    cb();
  };

  return {
    isLoggedIn,
    signin,
    signout,
  };
}

const ProvideAuth = ({ children }) => {
  const auth = useProvideAuth();
  return (
    <authContext.Provider value={auth}>
      {children}
    </authContext.Provider>
  );
}

export default ProvideAuth;
