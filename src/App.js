import React from 'react';
import { Slot } from './features/slot/Slot';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";
import PrivateRoute from './app/privateRoute';
import ProvideAuth from './app/auth';
import LoginPage from './features/login/Login';
import Home from './Home';
import { useSelector } from 'react-redux';
import RegisterPage from './features/register/Register';

function App() {

  const {
    username,
  } = useSelector(state => state.user.value);

  return (
    <div className="App">
      <ProvideAuth>
        <Router>
          <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
            <div className="container">
              <NavLink className="navbar-brand" to="/" exact>Slotmachine</NavLink>
              <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>

              <div className="collapse navbar-collapse" id="navbarNavDropdown">
                <ul className="navbar-nav me-auto">
                  <li className="nav-item">
                    <NavLink to="/" className="nav-link" exact>Home</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink to="/slot" className="nav-link" exact>Slot</NavLink>
                  </li>
                </ul>

                <span className="navbar-text">{username}</span>
              </div>
            </div>
          </nav>

          <div className="container p-4">
            <Switch>
              <PrivateRoute path="/slot">
                <Slot />
              </PrivateRoute>
              <Route path="/login">
                <LoginPage />
              </Route>
              <Route path="/register">
                <RegisterPage />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </div>
        </Router>
      </ProvideAuth>
    </div>
  );
}

export default App;
