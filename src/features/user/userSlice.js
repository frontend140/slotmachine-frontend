import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import Axios from "axios";

export const login = createAsyncThunk(
  'user/login',
  async (payload) => {
    const response = await fetchLogin(payload);
    return response();
  }
);

export const register = createAsyncThunk(
  'user/register',
  async (payload) => {
    const response = await fetchRegister(payload);
    return response();
  }
);

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    value: {
      isLoggedIn: !!localStorage.getItem('token'),
      username: localStorage.getItem('username'),
      email: localStorage.getItem('email'),
    },
  },
  reducers: {
    logout: state => {
      localStorage.removeItem('token');
      localStorage.removeItem('username');
      localStorage.removeItem('email');
      state.value = {
        isLoggedIn: false,
        username: null,
        email: null,
      };
    }
  },
  extraReducers: builder => {
    builder
      .addCase(login.fulfilled, (state, action) => {
        const {token, ...rest} = {...action.payload};
        localStorage.setItem('token', token);
        localStorage.setItem('username', rest.username);
        localStorage.setItem('email', rest.email);
        state.value = {
          ...state.value,
          ...rest,
          isLoggedIn: true,
        };
      })
  },
});

const fetchLogin = async (payload) => {
  return async () => {
    const response = await Axios.post(
      `${process.env.REACT_APP_API_URL}/login`,
      {...payload},
    );
    return response.data;
  }
}

const fetchRegister = async (payload) => {
  return async () => {
    const response = await Axios.post(
      `${process.env.REACT_APP_API_URL}/register`,
      { ...payload },
    );
    return response.data;
  }
}

export const { logout } = userSlice.actions;

export default userSlice.reducer;
