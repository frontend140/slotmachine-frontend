import { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { register } from "../user/userSlice";

import style from './Register.module.css';

const RegisterPage = () => {
  let history = useHistory();
  const dispatch = useDispatch();

  const [username, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const registerUser = e => {
    e.preventDefault();
    dispatch(register({ username, email, password }))
      .then(() => {
        history.replace('/login');
      })
      .catch(() => setError('Error'))
  };

  return (
    <div className={style.formRegister}>
      <form onSubmit={registerUser}>
        <div className="mb-2">
          <label className="form-label" htmlFor="username">Username</label>
          <input
            id="username"
            type="text"
            placeholder="Username"
            required
            className={`form-control ${error ? 'is-invalid' : ''}`}
            onChange={e => setUserName(e.currentTarget.value)} />
        </div>

        <div className="mb-2">
          <label className="form-label" htmlFor="email">E-mail</label>
          <input
            id="email"
            type="text"
            placeholder="E-mail"
            required
            className={`form-control ${error ? 'is-invalid' : ''}`}
            onChange={e => setEmail(e.currentTarget.value)} />
        </div>

        <div className="mb-2">
          <label className="form-label" htmlFor="password">Password</label>
          <input
            id="password"
            type="password"
            placeholder="Password"
            required
            className={`form-control ${error ? 'is-invalid' : ''}`}
            onChange={e => setPassword(e.currentTarget.value)} />
        </div>

        <button type="submit" className="btn btn-primary w-100">Register</button>
      </form>
    </div>
  );
}

export default RegisterPage;
