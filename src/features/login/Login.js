import { useState } from "react";
import { useHistory, useLocation } from "react-router";
import { Link } from "react-router-dom";
import { useAuth } from "../../app/privateRoute";

import style from './Login.module.css';

const LoginPage = () => {
  let history = useHistory();
  let location = useLocation();
  let auth = useAuth();

  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  let { from } = location.state || { from: { pathname: "/" } };
  let login = e => {
    e.preventDefault();
    auth.signin({username, password})
      .unwrap()
      .then(() => history.replace(from))
      .catch(() => setError('Invalid username/password combination'))
  };

  return (
    <div className={style.formLogin}>

      <form onSubmit={login}>
        <div className="mb-2">
          <label className="form-label" htmlFor="username">Username</label>
          <input
            id="username"
            type="text"
            required
            className={`form-control ${error ? 'is-invalid' : ''}`}
            onChange={e => setUserName(e.currentTarget.value)} />
        </div>

        <div className="mb-2">
          <label className="form-label" htmlFor="password">Password</label>
          <input
            id="password"
            type="password"
            required
            className={`form-control ${error ? 'is-invalid' : ''}`}
            onChange={e => setPassword(e.currentTarget.value)} />
        </div>

        { error ? <p className="alert alert-danger">{error}</p> : null }

        <button type="submit" className="btn btn-primary w-100 mb-4">Log in</button>
      </form>

      <p>Click <Link to="/register">here</Link> to register</p>
    </div>
  );
}

export default LoginPage;
