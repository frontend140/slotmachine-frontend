import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { spin, beforeSpin } from './slotSlice';

import styles from './Slot.module.css';

export const Slot = () => {
  const dispatch = useDispatch();

  const {
    coins,
    spinCombination,
    spinInProgress,
  } = useSelector(state => state.slot.value);

  const fruitIcons = {
    'lemon': '🍋',
    'cherry': '🍒',
    'banana': '🍌',
    'apple': '🍎',
  };

  useEffect(() => {
    const spaceSpin = e => { if (e.code === 'Space') makeSpin(); };
    window.addEventListener('keypress', spaceSpin);

    return function cleanup() {
      window.removeEventListener('keypress', spaceSpin);
    };
  });

  const makeSpin = () => {
    if (spinInProgress) return;

    document.querySelectorAll(`.${styles.reelIcon}`).forEach(element => {
      element.classList.add(styles.animate);
      element.classList.remove(styles.stop);
    });

    dispatch(beforeSpin());

    setTimeout(() => {
      dispatch(spin()).then(() => finishSpin());
    }, 3000);
  }

  const finishSpin = () => {
    document.querySelectorAll(`.${styles.reelIcon}`).forEach(element => {
      element.classList.add(styles.stop);
    });
  }

  return (
    <div>
      <div className={styles.slot}>
        <h1>Slot</h1>
        <p>Coins: 💰 {coins}</p>

        <div className={`mb-4 ${styles.slotReels}`}>
          <div className={styles.reel}>
            <div className={`${styles.reelIcon} ${styles.reelIcon1}`}>
              {fruitIcons[spinCombination[0]]}
            </div>
          </div>
          <div className={styles.reel}>
            <div className={`${styles.reelIcon} ${styles.reelIcon2}`}>
              {fruitIcons[spinCombination[1]]}
            </div>
          </div>
          <div className={styles.reel}>
            <div className={`${styles.reelIcon} ${styles.reelIcon3}`}>
              {fruitIcons[spinCombination[2]]}
            </div>
          </div>
        </div>

        <div>
          <button
            className="btn btn-lg btn-success"
            disabled={spinInProgress}
            onClick={() => makeSpin()}>SPIN</button>
          <br />
          <small>or press SPACEBAR</small>
        </div>
      </div>
    </div>
  );
}
