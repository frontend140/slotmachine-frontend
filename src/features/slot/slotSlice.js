import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import Axios from "axios";

export const spin = createAsyncThunk(
  'slot/spin',
  async () => {
    const response = await fetchSpin();
    return response();
  }
);

export const slotSlice = createSlice({
  name: 'slot',
  initialState: {
    value: {
      spinInProgress: false,
      coins: 20,
      spinCombination: ['banana', 'cherry', 'lemon'],
    },
  },
  reducers: {
    beforeSpin: state => {
      state.value.spinInProgress = true;
      state.value.coins += -1;
    }
  },
  extraReducers: builder => {
    builder
      .addCase(spin.fulfilled, (state, action) => {
        state.value = {
          ...state.value,
          ...action.payload,
          spinInProgress: false,
        };
      })
  },
});

export const fetchSpin = async () => {
  return async () => {
    const response = await Axios.get(`${process.env.REACT_APP_API_URL}/spin`);
    return response.data;
  }
}

export const { beforeSpin } = slotSlice.actions;

export default slotSlice.reducer;
